# TSRProject

Follow this steps to setup your project.

1. Create directory for project:

`mkdir LearningProject`

2. Switch to recently created directory:

`cd LearningProject`

3. Clone remote repository into your current directory:

`git clone https://gitlab.com/tsrproject/tsrproject.git`

4. Create new virtual environment:

`python3 -m venv tsr_venv`

5. Activate recently created virtual environment:

`source tsr_venv/bin/activate`

6. Open cloned repository:

`cd tsrproject`

7. Checkout to learning branch:

`git checkout learning`

8. Install requirements:

run `sudo apt install python3-pip` if pip is not installed

`pip install -r requirements.pip`

9. Install libraries:

run `sudo apt install npm` if npm is not installed

`npm install`

10. Build static (template files):

`webpack --progress --watch`

11. Leave webpack in separate terminal or close it (ctrl-z)

12. Create new branch for own development:

`git checkout -b {branch_name}` replace {branch_name} with your name (without braces)

Task list:

* [ ] Discover LinkedIn REST API: https://developer.linkedin.com/docs/rest-api
* [ ] Keys for oAuth as well as guide for python (if keys do not work - contact me): https://github.com/ozgur/python-linkedin
* [ ] Create forms for getting data (get user data from some social network if possible)
using API and to represent data. They should be on separate pages with different url.
* [ ] Root urls using tornado
* [ ] Install docker and build image of current app
* [ ] Deploy image to any free hosting